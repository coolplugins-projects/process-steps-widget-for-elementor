<?php
namespace ElementorPSWE\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Box_Shadow;

use Elementor\Group_Control_Image_Size;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class PSWE_widget extends Widget_Base {

	

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'process-steps';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Process Step', 'PSWE' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-sitemap';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'general' ];
	}

	 public function get_style_depends() {
		
		return ['PSWE-style-css'];
     }
	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends() {
		
		return ['PSWE-script-js'];

	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	
	   protected function _register_controls() {
		   
		/*process steps section start*/
		$this->start_controls_section(
			'section_process_steps',
			[
				'label' => esc_html__( 'Process/Steps', 'PSWE' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);
		$this->add_control(
			'PSWE_style',
			[
				'label' => esc_html__( 'Layout', 'PSWE' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'style_2',
				'options' => [
					'style_1'  => esc_html__( 'Vertical', 'PSWE' ),					
					'style_2' => esc_html__( 'Horizontal', 'PSWE' ),
				],
			]
		);
		
		$this->add_control(
			'PSWE_display_special_bg',
			[
				'label' => esc_html__( 'Highlighted Background', 'PSWE' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Enable', 'PSWE' ),
				'label_off' => esc_html__( 'Disable', 'PSWE' ),
				'default' => 'no',
				'separator' => 'before',				
			]
		);
		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'PSWE_loop_title',
			[
				'label' => esc_html__( 'Title', 'PSWE' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Process Steps', 'PSWE' ),
				'dynamic' => ['active'   => true,],
			]
		);
		$repeater->add_control(
			'PSWE_loop_image_icon',
			[
			'label' => esc_html__( 'Content Type', 'PSWE' ),
				'type' => Controls_Manager::SELECT,
				'description' => esc_html__(' select Icon, Image or Text','PSWE'),
				'default' => 'icon',
				'separator' => 'before',
				'options' => [					
					'icon' => esc_html__( 'Icon', 'PSWE' ),
					'image' => esc_html__( 'Image', 'PSWE' ),
					'text' => esc_html__( 'Text', 'PSWE' ),
				],
			]
		);
		$repeater->add_control(
			'PSWE_loop_select_image',
			[
				'label' => esc_html__( 'Use Image As icon', 'PSWE' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' =>'',
				],
				'media_type' => 'image',
				'dynamic' => [
					'active'   => false,
				],
				'condition' => [
					'PSWE_loop_image_icon' => 'image',
				],
			]
		);
		$repeater->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'pswe_thumbnail',
				'default' => 'full',
				'separator' => 'none',
				'separator' => 'after',
				'condition' => [
					'PSWE_loop_image_icon' => 'image',
				],
			]
		);
		$repeater->add_control(
			'PSWE_loop_icon_fontawesome',
			[
				'label' => esc_html__( 'Icon Library', 'PSWE' ),
				'type' => Controls_Manager::ICONS,
				'default' => [
					'value' => 'fa fa-tree',
					'library' => 'solid',
				],
				'condition' => [
					'PSWE_loop_image_icon' => 'icon',
					
				],	
			]
		);
		
		$repeater->add_control(
			'PSWE_loop_select_text',
			[
				'label' => esc_html__( 'Text', 'PSWE' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Step', 'PSWE' ),
				'dynamic' => ['active'   => true,],
				'condition' => [
					'PSWE_loop_image_icon' => 'text',
				],
			]
		);
		$repeater->add_control(
			'PSWE_loop_content_desc',
			[
				'label' => esc_html__( 'Description', 'PSWE' ),
				'type' => Controls_Manager::WYSIWYG,
				'default' => esc_html__('ipsum as it is sometimes known, is dummy text used in laying out print', 'PSWE' ),
			]
		);		
		
		
		$repeater->add_control(
			'PSWE_loop_icn_link',
			[
				'label' => esc_html__( 'Enable Read More', 'PSWE' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Enable', 'PSWE' ),
				'label_off' => esc_html__( 'Disable', 'PSWE' ),
				'default' => 'no',								
			]
		);
		$repeater->add_control(
			'PSWE_loop_url_link',
			[
				'label' => esc_html__( 'Link', 'PSWE' ),
				'type' => Controls_Manager::URL,
				'placeholder' => esc_html__( 'https://your-link.com', 'PSWE' ),
				
				'condition' => [
					'PSWE_loop_icn_link' => 'yes',
				],
			]
		);
		$repeater->add_control(
			'pswe_custom_text',
			[
				'label' => __( 'Custom Text', 'pswe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Read More', 'pswe' ),
				'placeholder' => __( 'Enter Custom Text', 'pswe' ),
				
				'condition' => [
					'PSWE_loop_icn_link' => 'yes',
				],
			]
		);
			$repeater->add_control(
			'pswe_background_color',
			[
				'label' => __( 'Background Color', 'pswe' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} {{CURRENT_ITEM}} .PSWE-left-imt .PSWE-icon-img' => 'background-color: {{VALUE}}',
				],
			]
		);
		$repeater->add_control(
			'pswe_background_hover_color',
			[
				'label' => __( 'Background Color Hover', 'pswe' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-wrapper:hover{{CURRENT_ITEM}} .PSWE-left-imt .PSWE-icon-img,
				{{WRAPPER}} .PSWE-process-steps-wrapper.active{{CURRENT_ITEM}} .PSWE-left-imt .PSWE-icon-img'=> 'background-color: {{VALUE}}',
				],
			]
		);

		
		$this->add_control(
            'loop_content',
            [
				'label' => esc_html__( 'Process/Steps', 'PSWE' ),
                'type' => Controls_Manager::REPEATER,
                'default' => [
                    [
                        'PSWE_loop_title' => 'Process 1',                       
                    ],
					[
                        'PSWE_loop_title' => 'Process 2',
                    ],
					[
                        'PSWE_loop_title' => 'Process 3',
                    ],					
                ],
                'separator' => 'before',
				'fields' => $repeater->get_controls(),
                'title_field' => '{{{ PSWE_loop_title }}}',				
            ]
        );
		$this->end_controls_section();
		/*process steps section start*/
		
		/* style section start*/
		/*title style start*/
		$this->start_controls_section(
            'section_title_styling',
            [
                'label' => esc_html__('Title Style', 'PSWE'),
                'tab' => Controls_Manager::TAB_STYLE,				
            ]
        );	
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .PSWE-process-steps-widget .PSWE-process-steps-wrapper .PSWE-content .PSWE-step-title',
			]
		);
		$this->add_control(
			'title_text_color_n',
			[
				'label' => esc_html__( 'Title Color', 'PSWE' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget .PSWE-process-steps-wrapper .PSWE-content .PSWE-step-title' => 'color: {{VALUE}};',
				],
			]
		);
		
		$this->end_controls_section();
		/*title style end*/
		
		/*description style start*/
		$this->start_controls_section(
            'section_description_styling',
            [
                'label' => esc_html__('Description Style', 'PSWE'),
                'tab' => Controls_Manager::TAB_STYLE,				
            ]
        );	
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'description_typography',
				'selector' => '{{WRAPPER}} .PSWE-process-steps-widget .PSWE-process-steps-wrapper .PSWE-content .PSWE-step-desc,{{WRAPPER}} .PSWE-process-steps-widget .PSWE-process-steps-wrapper .PSWE-content .PSWE-step-desc p,
				{{WRAPPER}} .PSWE-process-steps-widget .PSWE-process-steps-wrapper .PSWE-content .PSWE-step-desc span',
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'Read_more_Typography',
				'label' => __( 'Read-More-Typography', 'pswe' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .pswe_readmore_typo',
			]
		);
	
		$this->add_control(
			'title_description_color_n',
			[
				'label' => esc_html__( 'Description Color', 'PSWE' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget .PSWE-process-steps-wrapper .PSWE-content .PSWE-step-desc,{{WRAPPER}} .PSWE-process-steps-widget .PSWE-process-steps-wrapper .PSWE-content .PSWE-step-desc p' => 'color: {{VALUE}};',
				],
			]
		);
		
		$this->end_controls_section();
		/*description style end*/
		
		/*Icon/Image style start*/
		$this->start_controls_section(
            'section_icon_image_styling',
            [
                'label' => esc_html__('Icon/Image Style', 'PSWE'),
                'tab' => Controls_Manager::TAB_STYLE,				
            ]
        );
		
		$this->add_control(
			'tab_icon_heading',
			[
				'label' => esc_html__( 'Icon Options', 'PSWE' ),
				'type' => \Elementor\Controls_Manager::HEADING,				
			]
		);
		$this->add_responsive_control(
            'tab_icon_size',
            [
                'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Icon Size', 'PSWE'),
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 500,
						'step' => 1,
					],
				],				
				'render_type' => 'ui',
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget .PSWE-icon-img i' => 'font-size: {{SIZE}}{{UNIT}}',
				],
            ]
        );
		
		$this->add_control(
			'tab_icon_color_n',
			[
				'label' => esc_html__( 'Icon Color', 'PSWE' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget .PSWE-process-steps-wrapper .PSWE-icon-img i' => 'color: {{VALUE}};',
				],
			]
		);
			
		
		$this->add_control(
			'tab_image_heading',
			[
				'label' => esc_html__( 'Image Options', 'PSWE' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_responsive_control(
            'tab_image_size',
            [
                'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Image Size', 'PSWE'),
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 500,
						'step' => 1,
					],
				],
				'separator' => 'after',
				'render_type' => 'ui',
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget .PSWE-process-steps-wrapper .PSWE-icon-img .PSWE_icon-img' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}}',
				],
            ]
        );
		
		
		
		$this->add_control(
			'tab_bg_heading',
			[
				'label' => esc_html__( 'Background Options', 'PSWE' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_responsive_control(
            'tab_bg_size',
            [
                'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Background Size', 'PSWE'),
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 10,
						'max' => 500,
						'step' => 2,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 90,
				],
				'render_type' => 'ui',
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget .PSWE-process-steps-wrapper .PSWE-icon-img' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .PSWE-process-steps-widget .PSWE-left-imt .PSWE-special-bg:after' => 'width: calc({{SIZE}}{{UNIT}} + 20px);height:calc({{SIZE}}{{UNIT}} + 20px);',
					'{{WRAPPER}} .PSWE-process-steps-widget .PSWE-left-imt .PSWE-special-bg:before' => 'width: calc({{SIZE}}{{UNIT}} + 40px);height:calc({{SIZE}}{{UNIT}} + 40px);',
					'{{WRAPPER}} .PSWE-process-steps-widget.style_1 .PSWE-process-steps-wrapper .PSWE-left-imt:after,
					{{WRAPPER}} .PSWE-process-steps-widget.style_2 .PSWE-process-steps-wrapper .PSWE-left-imt:after' => 'left:calc(({{SIZE}}{{UNIT}} /2 ) - ({{seprator_border_width_n.SIZE}}px));',
					'{{WRAPPER}} .PSWE-process-steps-widget.style_1 .PSWE-process-steps-wrapper .PSWE-left-imt' => 'margin-right: calc(({{SIZE}}{{UNIT}}/1.3));',
					'{{WRAPPER}} .PSWE-process-steps-widget.style_1 .PSWE-process-steps-wrapper .PSWE-right-content' => 'width: calc((100% - ({{SIZE}}{{UNIT}} * 2)));',
				],
            ]
        );
		$this->add_responsive_control(
            'pro_ste_minimum_height',
            [
                'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Minimum Height of Content', 'PSWE'),
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 1000,
						'step' => 1,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 150,
				],
				'separator' => 'before',
				'render_type' => 'ui',
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget .PSWE-process-steps-wrapper,
					{{WRAPPER}} .PSWE-process-steps-widget.style_1 .PSWE-process-steps-wrapper .PSWE-left-imt:after' => 'min-height: {{SIZE}}{{UNIT}};',
				],
            ]
        );
		
		
		
		$this->end_controls_section();
		/*Icon/Image style end*/
		
		/*Separator/Line style start*/
		$this->start_controls_section(
            'section_seprator_styling',
            [
                'label' => esc_html__('Separator/Line Style', 'PSWE'),
                'tab' => Controls_Manager::TAB_STYLE,				
            ]
        );			
		$this->start_controls_tabs( 'tabs_seprator' );
		$this->start_controls_tab(
			'tab_seprator_n',
			[
				'label' => esc_html__( 'Normal', 'PSWE' ),
			]
		);
		$this->add_control(
			'seprator_color_n',
			[
				'label' => esc_html__( 'Color', 'PSWE' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget.style_1 .PSWE-process-steps-wrapper .PSWE-left-imt:after,
{{WRAPPER}} .PSWE-process-steps-widget.style_2 .PSWE-left-imt:before,
{{WRAPPER}} .PSWE-process-steps-widget.style_2 .PSWE-process-steps-wrapper .PSWE-left-imt:after' => 'border-color:{{VALUE}};',
				],
			]
		);
	
		$this->add_control(
			'seprator_border_style_n',
			[
				'label' => esc_html__( 'Border Type', 'PSWE' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'solid',
				'options' => [
                    'solid' => esc_html__('Solid', 'PSWE'),
                    'dashed' => esc_html__('Dashed', 'PSWE'),
                    'dotted' => esc_html__('Dotted', 'PSWE'),
                    'groove' => esc_html__('Groove', 'PSWE'),
                    'inset' => esc_html__('Inset', 'PSWE'),
                    'outset' => esc_html__('Outset', 'PSWE'),
                    'ridge' => esc_html__('Ridge', 'PSWE'),
                    
                ],
				'selectors'  => [
					'{{WRAPPER}} .PSWE-process-steps-widget.style_1 .PSWE-process-steps-wrapper .PSWE-left-imt:after,
{{WRAPPER}} .PSWE-process-steps-widget.style_2 .PSWE-left-imt:before,
{{WRAPPER}} .PSWE-process-steps-widget.style_2 .PSWE-process-steps-wrapper .PSWE-left-imt:after' => 'border-style: {{VALUE}};',
				],				
			]
		);
		$this->add_responsive_control(
            'seprator_border_width_n',
            [
                'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Border Size', 'PSWE'),
				'size_units' => [ 'px','%' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 1000,
						'step' => 1,
					],
					'%' => [
						'min' => 1,
						'max' => 100,
						'step' => 1,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 1,
				],
				'separator' => 'after',
				'render_type' => 'ui',
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget.style_2 .PSWE-process-steps-wrapper .PSWE-left-imt:before,
					{{WRAPPER}} .PSWE-process-steps-widget.style_1 .PSWE-process-steps-wrapper .PSWE-left-imt:after' => 'border-width: {{SIZE}}{{UNIT}} !important;',
				],
				'condition' => [					
					'seprator_border_style_n!' => 'border_img_custom',
				],
            ]
        );
		$this->add_control(
			'seprator_cusom_img',
			[
				'label' => esc_html__( 'Separator/Line Image', 'PSWE' ),
				'type' => Controls_Manager::MEDIA,
				'media_type' => 'image',
				'default' => [
					'url' => '',
				],
				'condition' => [
					'seprator_border_style_n' => 'border_img_custom',					
				],
			]
		);
		$this->add_responsive_control(
            'seprator_main_top_offset',
            [
                'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Separator/Line Offset', 'PSWE'),
				'size_units' => [ 'px','%' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 1000,
						'step' => 1,
					],
					'%' => [
						'min' => 1,
						'max' => 100,
						'step' => 1,
					],
				],
				'separator' => 'after',
				'render_type' => 'ui',
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget.style_1.tp_ps_sep_img .PSWE-custom-img-inner,
					{{WRAPPER}} .PSWE-process-steps-widget.style_2.tp_ps_sep_img .PSWE-custom-img-inner' => 'left: {{SIZE}}{{UNIT}} !important;position:relative;',
				],
				'condition' => [					
					'seprator_border_style_n' => 'border_img_custom',
				],
            ]
        );
		$this->add_responsive_control(
            'seprator_main_size',
            [
                'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Separator/Line Size', 'PSWE'),
				'size_units' => [ 'px','%' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 1000,
						'step' => 1,
					],
					'%' => [
						'min' => 1,
						'max' => 100,
						'step' => 1,
					],
				],
				'separator' => 'after',
				'render_type' => 'ui',
				'selectors' => [					
					'{{WRAPPER}} .PSWE-process-steps-widget.style_2 .PSWE-process-steps-wrapper .PSWE-left-imt:before' => 'width: {{SIZE}}{{UNIT}} !important;
	right: calc((-{{SIZE}}{{UNIT}} / 2) - 10px)!important;',
					'{{WRAPPER}} .PSWE-process-steps-widget.style_1.tp_ps_sep_img .PSWE-custom-img-inner' => 'max-height: {{SIZE}}{{UNIT}} !important;',
				],
				'condition' => [
					'PSWE_style!' => 'style_1',					
					'seprator_border_style_n!' => 'border_img_custom',					
				],
            ]
        );
		$this->add_responsive_control(
            'seprator_img_height_width',
            [
                'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Image Maximum Size', 'PSWE'),
				'size_units' => [ 'px','%' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 1000,
						'step' => 1,
					],
					'%' => [
						'min' => 1,
						'max' => 100,
						'step' => 1,
					],
				],
				'separator' => 'after',
				'render_type' => 'ui',
				'selectors' => [					
					'{{WRAPPER}} .PSWE-process-steps-widget.style_1.tp_ps_sep_img .PSWE-custom-img-inner' => 'max-height: {{SIZE}}{{UNIT}} !important;',
					'{{WRAPPER}} .PSWE-process-steps-widget.style_2.tp_ps_sep_img .PSWE-custom-img-inner' => 'width: {{SIZE}}{{UNIT}} !important;height:auto !important;max-width: {{SIZE}}{{UNIT}} !important;'
				],
				'condition' => [
					'seprator_border_style_n' => 'border_img_custom',					
				],
            ]
        );
		
		$this->add_responsive_control(
			'seprator_cusom_img_button_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'PSWE' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .PSWE-process-steps-widget.tp_ps_sep_img .PSWE-process-steps-wrapper .PSWE_separator_custom_img img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',				
				],	
				'condition' => [
					'seprator_border_style_n' => 'border_img_custom',					
				],
			]
		);
		$this->end_controls_tab();
		$this->start_controls_tab(
			'tab_seprator_h',
			[
				'label' => esc_html__( 'Hover', 'PSWE' ),
			]
		);
		$this->add_control(
			'seprator_color_h',
			[
				'label' => esc_html__( 'Color', 'PSWE' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget.style_1 .PSWE-process-steps-wrapper:hover .PSWE-left-imt:after,
{{WRAPPER}} .PSWE-process-steps-widget.style_2 .PSWE-process-steps-wrapper:hover .PSWE-left-imt:before,
{{WRAPPER}} .PSWE-process-steps-widget.style_2 .PSWE-process-steps-wrapper:hover .PSWE-left-imt:after,
{{WRAPPER}} .PSWE-process-steps-widget.style_1 .PSWE-process-steps-wrapper.active .PSWE-left-imt:after,
{{WRAPPER}} .PSWE-process-steps-widget.style_2 .PSWE-process-steps-wrapper.active .PSWE-left-imt:before,
{{WRAPPER}} .PSWE-process-steps-widget.style_2 .PSWE-process-steps-wrapper.active .PSWE-left-imt:after' => 'border-color:{{VALUE}};',
				],
			]
		);		
		$this->end_controls_tab();
		$this->end_controls_tabs();
		$this->end_controls_section();
		/*Separator/Line style end*/
		
		/*display counter start*/
		$this->start_controls_section(
            'section_display_counter_styling',
            [
                'label' => esc_html__('Display Counter Style', 'PSWE'),
                'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'PSWE_display_counter' => 'yes',					
				],
            ]
        );
		$this->add_responsive_control(
			'display_counter_padding',
			[
				'label' => esc_html__( 'Padding', 'PSWE' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em'],				
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-wrapper .PSWE_post_desc.dc_custom_text .ds_custom_text_label' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'PSWE_display_counter_style' => 'custom-text',					
				],
				'separator' => 'after',
			]
		);		

		$this->add_responsive_control(
            'display_counter_left_offset',
            [
                'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Left Offset', 'PSWE'),
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => -300,
						'max' => 300,
						'step' => 1,
					],
				],				
				'render_type' => 'ui',
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget .PSWE-left-imt .PSWE_post_desc' => 'margin-left: {{SIZE}}{{UNIT}}',
				],
            ]
        );
		$this->add_responsive_control(
            'display_counter_top_offset',
            [
                'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Top Offset', 'PSWE'),
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => -200,
						'max' => 200,
						'step' => 1,
					],
				],				
				'render_type' => 'ui',
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget .PSWE-left-imt .PSWE_post_desc' => 'margin-top: {{SIZE}}{{UNIT}}',
				],
            ]
        );
		$this->end_controls_section();
		/*display counter end*/
		
		/*Content Background Style start*/
		$this->start_controls_section(
            'section_content_bg_styling',
            [
                'label' => esc_html__('Content Background Style', 'PSWE'),
				'tab' => Controls_Manager::TAB_STYLE,	
				'condition' => [
					'PSWE_style' => 'style_1',					
				],			
            ]
        );
		
		$this->add_responsive_control(
            'content_bg_margin_right',
            [
                'type' => Controls_Manager::SLIDER,
				'label' => esc_html__('Left Content Right Margin', 'PSWE'),
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 300,
						'step' => 1,
					],
				],
				'separator' => 'after',
				'render_type' => 'ui',
				'selectors' => [
					'{{WRAPPER}} .PSWE-process-steps-widget.style_1 .PSWE-process-steps-wrapper .PSWE-left-imt' => 'margin-right: {{SIZE}}{{UNIT}} !important',
				],
				'condition' => [
					'PSWE_style' => 'style_1',					
				],
            ]
        );		
		
		$this->end_controls_section();
		/*Content Background Style end*/
		/* style section end*/
		
		
	}
    protected function render() {
		$settings = $this->get_settings_for_display();
		
		
			/*--Plus Extra ---*/
			
			$before_content =$after_content ='';
			$uid_widget=uniqid("plus");
		
		
			/*--Plus Extra ---*/
			$uid=uniqid('proste');
		
	$display_special_bg=$responsive_class=$seprator_cusom_img_class='';
		
		
		
		if(!empty($settings['seprator_border_style_n']) && $settings['seprator_border_style_n'] == 'border_img_custom'){
			$seprator_cusom_img_class = 'tp_ps_sep_img';
		}
		
		if(!empty($settings['PSWE_display_special_bg']) && $settings['PSWE_display_special_bg'] == 'yes'){
			$display_special_bg = 'PSWE-special-bg';
		}
		
			 if(!empty($settings["loop_content"])) {
				$output = '<div id="'.$uid.'" class="PSWE-process-steps-widget '.$settings['PSWE_style'].' '.$seprator_cusom_img_class.'  "  >';	
					$loop_content=$settings["loop_content"];
					$index=0;					
					foreach($loop_content as $index => $item) {
						$ps_count = $index;
						$on_load_class='';
						
						$list_title=$description=$title_a_start=$title_a_end=$list_img='';
						
						/*link*/
						if ( ! empty( $item['PSWE_loop_url_link']['url'] ) ) {
							$this->add_render_attribute( 'loop_box_link'.$index, 'href', $item['PSWE_loop_url_link']['url'] );
							if ( $item['PSWE_loop_url_link']['is_external'] ) {
								$this->add_render_attribute( 'box_link'.$index, 'target', '_blank' );
							}
							if ( $item['PSWE_loop_url_link']['nofollow'] ) {
								$this->add_render_attribute( 'box_link'.$index, 'rel', 'nofollow' );
							}
						}
						/*link*/
						
						/*tile*/
						
						if(!empty($item['PSWE_loop_title'])){							
							if (!empty($item['PSWE_loop_url_link']['url'])){
								$title_a_start = '<a '.$this->get_render_attribute_string( "loop_box_link".$index ).'>';
								$title_a_end = '</a>';

								
							}							
							$list_title = '<h6 class="PSWE-step-title">'.$item['PSWE_loop_title'].'</h6>';							
						}
						/*tile*/
						
						/*description*/	
						
							$customtetxt='';	
						if(!empty($item['PSWE_loop_content_desc'])){
							
							 $description='<div class="PSWE-step-desc"> '.$item['PSWE_loop_content_desc'].' </div>';
							 
						}	
						/*description*/
						
						/*icon-image-text*/
						if(!empty($item['PSWE_loop_image_icon'])){
							/*image*/
							if(isset($item['PSWE_loop_image_icon']) && $item['PSWE_loop_image_icon'] == 'image'){
								$image_alt='';
									
									if(!empty($item["PSWE_loop_select_image"]["url"])){
										$PSWE_loop_select_image=$item['PSWE_loop_select_image']['id'];
										$img = wp_get_attachment_image_src($PSWE_loop_select_image,$item['pswe_thumbnail_size']);
										$loop_imgSrc = $img[0];

										

										//$loop_imgSrc= $item["PSWE_loop_select_image"]["url"];
										$image_id=$item["PSWE_loop_select_image"]["id"];
										$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
										if(!$image_alt){
											$image_alt = get_the_title($image_id);
										}else if(!$image_alt){
											$image_alt = 'Image Icon';
										}
									}else{
										$loop_imgSrc='';
									}
									
									$list_img ='<div class="PSWE-icon-img PSWE-step-icon-img" >';
										$list_img .='<img class="PSWE_icon-img" src='.esc_url($loop_imgSrc).'  />';
									$list_img .='</div>';
							}
							/*image*/							
							
							/*text*/
							else if(isset($item['PSWE_loop_image_icon']) && $item['PSWE_loop_image_icon'] == 'text'){
								$list_img='<span class="PSWE-text">'.$item['PSWE_loop_select_text'].'</span>';
							}
							
							
						}
						/*icon-image-text*/
						if(isset($item['PSWE_loop_image_icon']) && $item['PSWE_loop_image_icon'] == 'icon'){	
							ob_start();
							\Elementor\Icons_Manager::render_icon( $item['PSWE_loop_icon_fontawesome'], [ 'aria-hidden' => 'true' ]);
							$list_img = ob_get_contents();
							ob_end_clean();						
						}
						
						
						
						$dis_sep_custom_img='';
						if(!empty($settings['seprator_border_style_n']) && $settings['seprator_border_style_n'] == 'border_img_custom'){							
							$dis_sep_custom_img='<span class="PSWE_separator_custom_img"><img class="PSWE-custom-img-inner" src="'.$settings['seprator_cusom_img']['url'].'" /></span>';
						}						
						if (!empty($item['PSWE_loop_url_link']['url'])){
								 $custom_a_start = '<a '.$this->get_render_attribute_string( "loop_box_link".$index ).'>';
								$custom_a_end = '</a>';
							 $customtetxt='<span class="pswe_readmore_typo">'.$custom_a_start.''.$item['pswe_custom_text'].''.$custom_a_end.'</span>';
							 }
						$output .= '<div class="PSWE-process-steps-wrapper elementor-repeater-item-' . $item['_id'] . ' elementor-ps-content-'.$ps_count.' '.$on_load_class.'" data-index="'.$ps_count.'">';
							if(!empty($settings['PSWE_style'])){
								if($settings['PSWE_style']=='style_1' || $settings['PSWE_style']=='style_2'){									
									$output .= '<div class="PSWE-left-imt '.$display_special_bg.'">';	
													
													$icn_a_start=$icn_a_end='';
													if(!empty($item['PSWE_loop_icn_link'] && $item['PSWE_loop_icn_link'] == 'yes')){					
														if (!empty($item['PSWE_loop_url_link']['url'])){
															$icn_a_start = '<a '.$this->get_render_attribute_string( "loop_box_link".$index ).'>';
															$icn_a_end = '</a>';
														}
													}
													if(!empty($list_img)){
														$output .= ''.$dis_sep_custom_img.'<span class="PSWE-icon-img '.$display_special_bg.'">'.$list_img.'</span> ';
													}
									$output .= '</div>';									
									$output .= '<div class="PSWE-right-content">';
										$output .= '<span class="PSWE-content">'.$list_title.' '.$description. $customtetxt.'</span>';
									$output .= '</div>';
								}
							}
						$output .= '</div>';
						$index++;
					}
					
				$output .= '</div>';				
				echo $before_content.$output.$after_content;
			}
	}


			protected function _content_template() {
					?>

		
			<#
			var before_content ='';
			var after_content ='';
			var uid_widget=Math.random();		
		    var uid=Math.random();		    
		    var display_special_bg,responsive_class,seprator_cusom_img_class,output;		    
		    if(settings.seprator_border_style_n == 'border_img_custom'){
		    	seprator_cusom_img_class = 'tp_ps_sep_img';
		    }
		    
		    if(settings.PSWE_display_special_bg == 'yes'){
		    	display_special_bg = 'PSWE-special-bg';
		    }
		    
		    	 if(settings.loop_content) {
		    		 #>
		    		 
		    		<div id="{{{uid}}}" class="PSWE-process-steps-widget {{{settings.PSWE_style}}} {{{seprator_cusom_img_class}}}  "  >	
		    		<#
		    		var loop_content=settings.loop_content;
		    			var index=0;
		    			  _.each( loop_content, function( item, index ) {					
		    			
		    				var ps_count = index;
		    				var on_load_class='';						
		    				var list_title,description,title_a_start,title_a_end,list_img;						
		    				var dis_sep_custom_img='';
		    				if(settings.seprator_border_style_n == 'border_img_custom'){							
		    					dis_sep_custom_img='<span class="PSWE_separator_custom_img"><img class="PSWE-custom-img-inner" src="{{{settings.seprator_cusom_img.url}}}" /></span>';
		    				}						
		    					#>
		    				<div class="PSWE-process-steps-wrapper elementor-repeater-item-{{{item._id}}} elementor-ps-content-{{{ps_count}}} {{{on_load_class}}}" data-index="{{{ps_count}}}">
		    					<#
		    					if(settings.PSWE_style){
		    						if(settings.PSWE_style=='style_1' || settings.PSWE_style=='style_2'){			
		    							#>						
		    							<div class="PSWE-left-imt {{{display_special_bg}}}">		    											
		    										
		    											<span class="PSWE-icon-img {{{display_special_bg}}}">
		    												{{{dis_sep_custom_img}}}
		    												<#
		    														if(item.PSWE_loop_image_icon == 'text'){
		    												#>
		    												 <span class="PSWE-text">{{{item.PSWE_loop_select_text}}}</span>
		    												 <#
		    													}
		    													if(item.PSWE_loop_image_icon == 'icon'){	

		    												 #>
		    													<i class="{{{item.PSWE_loop_icon_fontawesome.value}}}"></i>
		    													<#
		    													}
		    													if(item.PSWE_loop_image_icon == 'image'){
		    														
		    															  var process_image = {
                                                                                               id: item.PSWE_loop_select_image.id,
                                                                                               url: item.PSWE_loop_select_image.url,
                                                                                               size: item.pswe_thumbnail_size,
                                                                                               dimension: item.pswe_thumbnail_custom_dimension,
                                                                                               
                                                                                           };
		    														var pswe_image_url = elementor.imagesManager.getImageUrl( process_image );
		    														
		    														
		    													#>
		    													<div class="PSWE-icon-img PSWE-step-icon-img" ><img class="PSWE_icon-img" src="{{{pswe_image_url}}}" alt="{{{pswe_image_url}}}" /></div>
		    													<#
		    													}
		    													 
		    													#>
		    												
		    												</span>
		    										
		    							</div>								
		    							<div class="PSWE-right-content">
		    								
		    							<span class="PSWE-content">
		    								<h6 class="PSWE-step-title">{{{item.PSWE_loop_title}}}</h6>	
		    								<div class="PSWE-step-desc"> {{{item.PSWE_loop_content_desc}}}
												<#
												if(item.PSWE_loop_icn_link=='yes' && item.PSWE_loop_url_link.url!==''){ 
												#>
													<br><p class="pswe_readmore_typo"><a href="{{{item.PSWE_loop_url_link.url}}}">{{{item.pswe_custom_text}}}</a></p>
												<#
												}
												#>
		    								 </div>	
		    								</span>
		    							</div>
		    							<#
		    						}
		    					}
		    				#></div>
		    				<#
		    				index++;
		    			})
		    			#>
		    		</div>	
		    			
		    		<#
    
		    } 
		     #>
		    <?php
	}





}